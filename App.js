/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ImageBackground, Image, TextInput, Dimensions, TouchableOpacity} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

import bgImage from './images/background.jpeg'
import logo from './images/logo.jpg'
import Icon from 'react-native-vector-icons/Ionicons'

const { width: WIDTH } = Dimensions.get('window')

export default class Appbeta extends Component{
  render(){
    return (
        <ImageBackground source={bgImage} style={styles.backgroundContainer}>
            <View style={styles.logoContainer}>
              <Image source={logo} style={styles.logo}/>
              <Text style={styles.logoText}>REACT NATIVE</Text>
            </View>

            <View style={styles.inputContainer}>
              <Icon name={'ios-person'} size={28} color={'rgba(255, 255, 255, 0.7)'}
              style={styles.inputIcon}
              />
              <TextInput 
                style={styles.input}
                placeholder= {'Username'}
                placeholderTextColor={'rgba(255, 255, 255, 0.7)'}
                underlineColorAndroid='transparent'
              />
            </View>

            <View style={styles.inputContainer}>
              <Icon name={'ios-lock'} size={28} color={'rgba(255, 255, 255, 0.7)'}
              style={styles.inputIcon}
              />
              <TextInput 
                style={styles.input}
                placeholder= {'Password'}
                secureTextEntry= {true}
                placeholderTextColor={'rgba(255, 255, 255, 0.7)'}
                underlineColorAndroid='transparent'
              />

              <TouchableOpacity style={styles.btnEye}>
                  <Icon name={'ios-eye'} size={26} color={'rgba(255, 255, 255, 0.7)'} />
              </TouchableOpacity>
            </View>

            <TouchableOpacity style={styles.btnLogin}>
              <Text style={styles.text}>Login</Text>
            </TouchableOpacity>
        </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundContainer: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoContainer: {
    alignItems: 'center',
    marginBottom: 50,
  },
  logo: {
    width: 120,
    height: 120,
  },
  logoText: {
    color: 'white',
    fontSize: 20,
    fontWeight: '500',
    marginTop: 10,
    opacity : 0.5
  },
  input: {
    width : WIDTH - 5,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(0, 0, 0, 0.35)',
    color: 'rgba(255, 255, 25,0.7)',
    marginHorizontal: 25,
  },
  inputContainer: {
    marginTop: 10,
  },
  inputIcon: {
    position: 'absolute',
    top: 8,
    left: 37,
  },
  btnEye:{
    position: 'absolute',
    top: 8,
    right: 37,
  }, 
  btnLogin: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#432577',
    justifyContent: 'center',
    marginTop: '20',
  }
});
